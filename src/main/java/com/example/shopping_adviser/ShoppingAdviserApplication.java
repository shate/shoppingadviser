package com.example.shopping_adviser;

import com.example.shopping_adviser.service.JwtFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ShoppingAdviserApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShoppingAdviserApplication.class, args);
    }



}
