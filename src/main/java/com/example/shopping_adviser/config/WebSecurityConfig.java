package com.example.shopping_adviser.config;

import com.example.shopping_adviser.service.JwtFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder(BCryptPasswordEncoder.BCryptVersion.$2Y, 12);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable().authorizeRequests()
                .antMatchers(HttpMethod.GET, "/api/customer", "/api/customer/all").hasAnyRole("ADMIN", "CUSTOMER")
                .antMatchers(HttpMethod.POST, "/api/admin/product", "/api/admin/customer").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT, "/api/admin/product", "/api/admin/customer", "/api/admin/order").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH, "/api/admin/product", "/api/admin/customer", "/api/admin/order").hasRole("ADMIN")
                .and()
                .addFilter(new JwtFilter(authenticationManager()));

    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers("/console/**")
                .antMatchers(HttpMethod.POST, "/api/getToken", "/api/order")
                .antMatchers(HttpMethod.GET,"/api/product", "/api/product/all",  "/api/order", "/api/order/all");
    }
}
