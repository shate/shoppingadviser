package com.example.shopping_adviser.controller;

import com.example.shopping_adviser.dao.entity.Customer;
import com.example.shopping_adviser.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class CustomerController {

    private CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService){
        this.customerService = customerService;
    }

    @GetMapping("/customer/all")
    public Iterable<Customer> getAll(){
        return customerService.findAll();
    }

    @GetMapping("/customer")
    public Optional<Customer> getById(@RequestParam Long id){
        return customerService.findById(id);
    }

    @PostMapping("/admin/customer")
    public Customer addCustomer(@RequestBody Customer customer){
        return customerService.save(customer);
    }

    @PutMapping("/admin/customer")
    public Customer updateCustomer(@RequestParam Long id, @RequestBody Customer customer){
        Customer c = customerService.findById(id).get();
        c.setName(customer.getName());
        c.setAddress(customer.getAddress());
        return customerService.save(c);
    }

    @DeleteMapping("/admin/customer")
    public void deleteCustomer(@RequestParam Long id){
        customerService.deleteById(id);
    }

    @PatchMapping(path = "/admin/customer")
    public Customer updateCustomer(@RequestParam Long id, @RequestBody Map<String, String> formParams) {
        System.out.println(formParams);
        return customerService.updateOrder(id, formParams);
    }
}
