package com.example.shopping_adviser.controller;
import com.example.shopping_adviser.dao.entity.Order;
import com.example.shopping_adviser.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class OrderController {

    private OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService){
        this.orderService = orderService;
    }

    @GetMapping("/order/all")
    public Iterable<Order> getAll(){
        return orderService.findAll();
    }

    @GetMapping("/order")
    public Optional<Order> getById(@RequestParam Long id){
        return orderService.findById(id);
    }

    @PostMapping("/order")
    public Order addOrder(@RequestBody Map<String, Object> content){
        return orderService.save(content);
    }

    @PutMapping("/admin/order")
    public Order updateWholeOrder(@RequestParam Long id, @RequestBody Map<String, Object> content){
        /*
        Order o = orderService.findById(id).get();
        o.setCustomer(order.getCustomer());
        o.setProducts(order.getProducts());
        o.setPlaceDate(order.getPlaceDate());
        o.setStatus(order.getStatus());

         */
        return orderService.updateWholeOrder(id, content);
    }

    @DeleteMapping("/admin/order")
    public void deleteOrder(@RequestParam Long id){
        orderService.deleteById(id);
    }

    @PatchMapping(path = "/admin/order")
    public Order updateOrder(@RequestParam Long id, @RequestBody Map<String, Object> formParams) {
        return orderService.updateOrder(id, formParams);
    }

}

