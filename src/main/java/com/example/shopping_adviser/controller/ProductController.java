package com.example.shopping_adviser.controller;

import com.example.shopping_adviser.dao.entity.Product;
import com.example.shopping_adviser.service.ProductService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ProductController {

    private ProductService productService;

    @Autowired
    public ProductController(ProductService productService){
        this.productService = productService;
    }

    @GetMapping("/product/all")
    public Iterable<Product> getAll(){
        return productService.findAll();
    }

    @GetMapping("/product")
    public Optional<Product> getById(@RequestParam Long id){
        return productService.findById(id);
    }

    @PostMapping("/admin/product")
    public Product addProduct(@RequestBody Product product){
        return productService.save(product);
    }

    @PutMapping("/admin/product")
    public Product updateProduct(@RequestParam Long id, @RequestBody Product product){
        Product p = productService.findById(id).get();
        p.setName(product.getName());
        p.setPrice(product.getPrice());
        p.setAvailable(product.isAvailable());
        return productService.save(p);
    }

    @DeleteMapping("/admin/product")
    public void deleteProduct(@RequestParam Long id){
        productService.deleteById(id);
    }

    @PatchMapping(path = "/admin/product")
    public Product updateProduct(@RequestParam Long id, @RequestBody Map<String, String> formParams) {
        System.out.println(formParams);
        return productService.updateProduct(id, formParams);
    }

}
