package com.example.shopping_adviser.controller;

import com.example.shopping_adviser.dao.entity.UserDto;
import com.example.shopping_adviser.service.UserDtoService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class TokenController {
    UserDtoService userDtoService;
    PasswordEncoder passwordEncoder;

    @Autowired
    public TokenController(UserDtoService userDtoService, PasswordEncoder passwordEncoder) {
        this.userDtoService = userDtoService;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping("/getToken")
    public String getToken(@RequestBody Map<String, String> userData){
        if(userData.containsKey("name") && userData.containsKey("password")) {
            String name = userData.get("name");
            String password = userData.get("password");
            Optional<UserDto> userDto = userDtoService.findByName(name);
            long nowMillis = System.currentTimeMillis();

            if(userDto.isEmpty()){
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
            } else {
                UserDto user = userDto.get();
                if(passwordEncoder.matches(password, user.getPasswordHash())){
                    Date now = new Date(nowMillis);
                    Date expDate = new Date(nowMillis + 200000L);
                    String token = Jwts.builder()
                            .setSubject(user.getName())
                            .claim("role", user.getRole())
                            .claim("name", name)
                            .claim("password", password)
                            .setIssuedAt(now)
                            .setExpiration(expDate)
                            .signWith(SignatureAlgorithm.HS512, "pass")
                            .compact();
                    return token;
                }else{
                    throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Wrong password");
                }
            }

        }else{
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Name and password needed");
        }
    }
}
