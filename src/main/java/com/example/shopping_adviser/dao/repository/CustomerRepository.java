package com.example.shopping_adviser.dao.repository;

import com.example.shopping_adviser.dao.entity.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long> {
}
