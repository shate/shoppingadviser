package com.example.shopping_adviser.dao.repository;

import com.example.shopping_adviser.dao.entity.UserDto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserDtoRepository extends CrudRepository<UserDto, Long> {
    Optional<UserDto> findByName(String name);
}
