package com.example.shopping_adviser.service;

import com.example.shopping_adviser.dao.entity.Customer;
import com.example.shopping_adviser.dao.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.Optional;

@Service
public class CustomerService {

    private CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    // Pobieranie po ID
    public Optional<Customer> findById(Long id){
        return customerRepository.findById(id);
    }

    // Pobieranie wszystkich
    public Iterable<Customer> findAll(){
        return customerRepository.findAll();
    }

    // Dodawanie customera
    public Customer save(Customer customer){
        return customerRepository.save(customer);
    }

    // Usuwanie customera po ID
    public void deleteById(Long id){
        customerRepository.deleteById(id);
    }

    public Customer updateOrder(Long id, Map<String, String> content) {
        if (customerRepository.findById(id).isEmpty()) {
            throw new IllegalArgumentException("The Customer does not exist for id: " + id);
        }
        Customer customer = customerRepository.findById(id).orElseThrow();
        for(String name : content.keySet()){
            if(name.equals("name")){
                customer.setName(content.get("name"));
            }else if(name.equals("address")){
                customer.setAddress(content.get("address"));
            }
        }
        return customerRepository.save(customer);
    }
}
