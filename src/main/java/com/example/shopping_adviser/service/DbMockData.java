package com.example.shopping_adviser.service;

import com.example.shopping_adviser.dao.entity.*;
import com.example.shopping_adviser.dao.repository.CustomerRepository;
import com.example.shopping_adviser.dao.repository.OrderRepository;
import com.example.shopping_adviser.dao.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Component
public class DbMockData {
    private ProductService productService;
    private CustomerService customerService;
    private OrderService orderService;
    private UserDtoService userDtoService;
    private UserDtoBuilder userDtoBuilder;

    @Autowired
    public DbMockData(ProductService productService, CustomerService customerService, OrderService orderService, UserDtoService userDtoService, UserDtoBuilder userDtoBuilder) {
        this.productService = productService;
        this.customerService = customerService;
        this.orderService = orderService;
        this.userDtoService = userDtoService;
        this.userDtoBuilder = userDtoBuilder;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fill(){
        Product p1 = new Product("Flower", 4.55f, true);
        Product p2 = new Product("Apple", 0.55f, true);
        Product p3 = new Product("Salad", 1.10f, true);
        Product p4 = new Product("Coca-cola", 2.45f, true);



        Customer customer = new Customer("El Profesor", "Spain");
        Customer customer2 = new Customer("Cooper", "America");

        Set<Product> products = new HashSet<>(Arrays.asList(p1, p2, p3, p4));

        Order order = new Order(customer, products, LocalDateTime.now(), "in progress");

        productService.save(p1);
        productService.save(p2);
        productService.save(p3);
        productService.save(p4);


        customerService.save(customer);
        customerService.save(customer2);

        orderService.save(order);

        User customerUser = new User("customer", "customer", "ROLE_CUSTOMER");
        User adminUser = new User("admin", "admin", "ROLE_ADMIN");

        UserDto customerU = userDtoBuilder.createUser(customerUser);
        UserDto adminU = userDtoBuilder.createUser(adminUser);

        userDtoService.save(customerU);
        userDtoService.save(adminU);
    }
}
