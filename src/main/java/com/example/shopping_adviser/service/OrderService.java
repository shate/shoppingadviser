package com.example.shopping_adviser.service;

import com.example.shopping_adviser.dao.entity.Customer;
import com.example.shopping_adviser.dao.entity.Order;
import com.example.shopping_adviser.dao.entity.Product;
import com.example.shopping_adviser.dao.repository.OrderRepository;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class OrderService {
    private OrderRepository orderRepository;
    private CustomerService customerService;
    private ProductService productService;

    @Autowired
    public OrderService(OrderRepository orderRepository, CustomerService customerService, ProductService productService) {
        this.orderRepository = orderRepository;
        this.customerService = customerService;
        this.productService = productService;
    }

    /**
     * For | GET api/product?id={id}  |request
     * @param id ID of requested Order
     * @return Order if exists
     */
    public Optional<Order> findById(Long id){
        return orderRepository.findById(id);
    }

    /**
     * For | GET api/product/all  | request
     * @return All orders
     */
    public Iterable<Order> findAll(){
        return orderRepository.findAll();
    }

    /**
     * For | POST api/order  | request
     * @param content Map with proper values (to create an Order object)
     * @return Created Order object
     */
    public Order save(Map<String, Object> content){
        Order order = new Order();
        if(content.keySet().size() != 4){
            throw new IllegalArgumentException("The order contains 4 fields");
        }
        mapToOrder(content, order);
        return orderRepository.save(order);
    }

    /**
     * For initializing data
     * @param order Order to put into DB
     * @return Put Order
     */
    public Order save(Order order){
        return orderRepository.save(order);
    }

    /**
     * For | DELETE api/admin/order?id={id} | request
     * @param id ID of Order to delete
     */
    public void deleteById(Long id){
        orderRepository.deleteById(id);
    }

    /**
     * For | PUT api/admin/order?id={id} | request
     * @param id ID of Order to update
     * @param content Map with proper values (to create an Order object)
     * @return Updated Order object
     */
    public Order updateOrder(Long id, Map<String, Object> content) {
        if (orderRepository.findById(id).isEmpty()) {
            throw new IllegalArgumentException("The Order does not exist for id: " + id);
        }
        Order order = orderRepository.findById(id).orElseThrow();
        mapToOrder(content, order);
        return orderRepository.save(order);
    }

    /**
     * For | PATCH api/admin/order?id={id} | request
     * @param id ID of updated Order
     * @param content Map with proper values (to create an Order object)
     * @return Updated Order object
     */
    public Order updateWholeOrder(Long id, Map<String, Object> content) {
        if (orderRepository.findById(id).isEmpty()) {
            throw new IllegalArgumentException("The Order does not exist for id: " + id);
        }
        if(content.keySet().size() != 4){
            throw new IllegalArgumentException("The order contains 4 fields");
        }
        Order order = orderRepository.findById(id).orElseThrow();

        mapToOrder(content, order);
        return orderRepository.save(order);
    }

    /**
     * For mapping Map with key:value to Order object
     * @param content Map with proper values (to create an Order object)
     * @param order Order object
     */
    private void mapToOrder(Map<String, Object> content, Order order){
        Gson g = new Gson();
        for(String name : content.keySet()){
            switch (name) {
                case "customerID":
                    Long identifier = ((Number) content.get("customerID")).longValue();
                    Customer customer = customerService.findById((identifier)).get();
                    //Customer customer = g.fromJson(content.get("customer"), Customer.class);
                    order.setCustomer(customer);
                    break;
                case "productsID":
                    JsonArray ids = g.fromJson(String.valueOf(content.get("productsID")), JsonArray.class);
                    Set<Product> productSet = new HashSet<>();
                    for (JsonElement id : ids) {
                        Long idid = g.fromJson(id, Long.class);
                        productSet.add(productService.findById(idid).get());
                    }

                    System.out.println(productSet);

                    //g.fromJson(content.get("products"), Set.class);
                    order.setProducts(productSet);
                    break;
                case "placeDate":
                    LocalDateTime dateTime = LocalDateTime.parse((CharSequence) content.get("placeDate"));
                    order.setPlaceDate(dateTime);
                    break;
                case "status":
                    order.setStatus((String) content.get("status"));
                    break;
            }
        }
    }
}
