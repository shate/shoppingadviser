package com.example.shopping_adviser.service;

import com.example.shopping_adviser.dao.entity.Product;
import com.example.shopping_adviser.dao.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.Optional;

@Service
public class ProductService {

    private ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    // Pobieranie po ID
    public Optional<Product> findById(Long id){
        return productRepository.findById(id);
    }

    // Pobieranie wszystkich
    public Iterable<Product> findAll(){
        return productRepository.findAll();
    }

    // Dodawanie producta
    public Product save(Product product){
        return productRepository.save(product);
    }

    // Usuwanie producta po ID
    public void deleteById(Long id){
        productRepository.deleteById(id);
    }

    public Product updateProduct(Long id, Map<String, String> content) {
        if (productRepository.findById(id).isEmpty()) {
            throw new IllegalArgumentException("The Customer does not exist for id: " + id);
        }
        Product product = productRepository.findById(id).orElseThrow();
        for(String name : content.keySet()){
            if(name.equals("name")){
                product.setName(content.get("name"));
            }else if(name.equals("price")){
                product.setPrice(Float.parseFloat(content.get("price")));
            }else if(name.equals("available")){
                product.setAvailable(Boolean.parseBoolean(content.get("available")));
            }
        }
        return productRepository.save(product);
    }
}
