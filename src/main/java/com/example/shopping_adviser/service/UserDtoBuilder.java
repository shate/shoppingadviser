package com.example.shopping_adviser.service;

import com.example.shopping_adviser.dao.entity.User;
import com.example.shopping_adviser.dao.entity.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserDtoBuilder {
    PasswordEncoder passwordEncoder;

    @Autowired
    public UserDtoBuilder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public UserDto createUser(User user){
        UserDto userDto = new UserDto();
        userDto.setName(user.getName());
        userDto.setRole(user.getRole());
        String passwordHash = passwordEncoder.encode(user.getPassword());
        userDto.setPasswordHash(passwordHash);
        return userDto;
    }
}
