package com.example.shopping_adviser.service;

import com.example.shopping_adviser.dao.entity.UserDto;
import com.example.shopping_adviser.dao.repository.UserDtoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserDtoService {
    private UserDtoRepository userDtoRepository;

    @Autowired
    public UserDtoService(UserDtoRepository userDtoRepository) {
        this.userDtoRepository = userDtoRepository;
    }

    public Optional<UserDto> findByName(String name){
        return userDtoRepository.findByName(name);
    }

    public Optional<UserDto> findById(Long id){
        return userDtoRepository.findById(id);
    }

    public void deleteById(Long id){
        userDtoRepository.deleteById(id);
    }

    public boolean save(UserDto userDto){
        if(findByName(userDto.getName()).isEmpty()) {
            System.out.println("User successfully added");
            userDtoRepository.save(userDto);
            return true;
        }else{
            System.out.println("User already exists");
            return false;
        }
    }


}
